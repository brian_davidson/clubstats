/*
  A ping pong bot, whenever you send "ping", it replies "pong".
*/

// Import the discord.js module
const { prefix, token } = require('./config.json');
const Discord = require('discord.js');
const axios = require('axios')

// Create an instance of a Discord client
const client = new Discord.Client();

// The token of your bot - https://discordapp.com/developers/applications/me

// The ready event is vital, it means that your bot will only start reacting to information
// from Discord _after_ ready is emitted
client.on('ready', () => {
    console.log('I am ready!')
});

// Create an event listener for messages
client.on('message', message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(' ');
    const command = args.shift().toLowerCase();

    let option = args[0]

    console.log(option)

    // show club stats
    if (option === `club`) {
        var text = '```javascript\n'
        axios.get('https://www.easports.com/iframe/fifa17proclubs/api/platforms/PS4/clubs/1947018/stats')
            .then(function(response) {
                for (var datum in response.data.raw) {
                    for (var key in response.data.raw[datum]) {
                        let obj = response.data.raw[datum]
                        text = text + key + ': ' + obj[key] + '\n'
                    }
                }
                text = text + '```'
                message.channel.send(text)
            })
            .catch(function(error) {
                console.log(error)
            })
    } else {
        var text = '```javascript\n'
        axios.get('https://www.easports.com/iframe/fifa17proclubs/api/platforms/PS4/clubs/1947018/membersComplete')
            .then(function(response) {
                for (var datum in response.data.raw) {
                    let player = response.data.raw[datum]
                    if (player.name === option) {
                        for (var stat in player) {
                            text = text + stat + ': ' + player[stat] + '\n'
                        }
                        text = text + '```'
                        message.channel.send(text)
                        text = '```javascript\n'
                    }
                }
            })
            .catch(function(error) {
                console.log(error)
            })
    }
});

// Log our bot in
client.login(token);
